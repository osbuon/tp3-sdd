#ifndef ABR_H
#define ABR_H


#define ABR_TYPE int

typedef struct abr_t {
    ABR_TYPE value;
    struct abr_t * left;
    struct abr_t * right;
} ABR;


#endif